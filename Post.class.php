<?php
/**
 * Author: Maksym Kawelski
 * Date: 15.09.18
 * Time: 18:31
 *
 * Post class object
 *
 */

class Post
{

    # Post.class variables
    private $id = null;
    private $title = null;
    private $author = null;
    private $date = null;
    private $contents = null;
    private $status = null;

    # Helping variables
    private $gotPost = null; # true when post by id exist; false when not
    private $errorMsg = Array(); # Errors array
    private $dbHandle = null; # DB handle


    public function __construct($idPost = null)
    {

        require_once "Db.class.php";

        global $dbHandle;

        $this->dbHandle = $dbHandle;

        # Check id Post.class
        if ( ($idPost != null) and (is_numeric($idPost)) )
        {

            # Get post data from db
            $this->dbHandle->bind("id",$idPost);
            $post = $this->dbHandle->query("SELECT * FROM `post` WHERE `id` = :id");

            # Post.class exist
            if (count($post) == 1)
            {

                $this->id = $post[0]['id'];
                $this->author = $post[0]['author'];
                $this->title = $post[0]['title'];
                $this->date = $post[0]['date'];
                $this->contents = $post[0]['contents'];
                $this->status = $post[0]['status'];

                $this->gotPost = true;

            }
            # Error - post don't exist
            else $this->gotPost = false;

        }
        # Error - bad id post
        else $this->gotPost = false;

    }

    /*
     *
     * Get data functions
     *
     */

    public function getId ()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDate ()
    {
        return $this->date;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getContents()
    {
        return $this->contents;
    }

    public function gotPost ()
    {
        return $this->gotPost;
    }

    public function getErrorMsg ()
    {
        return $this->errorMsg;
    }

    /*
     *
     * Validation data functions
     *
     */

    private function validationAuthor($author)
    {

        # Variable is not empty
        if (!empty($author))
        {

            # Variable is longer than 50 characters
            if(strlen($author) > 50)
            {
                $this->errorMsg[] = 4;
            }

        }
        # Error - empty variable
        else
        {
            $this->errorMsg[] = 5;
        }

    }

    private function validationTitle($title)
    {

        # Variable is not empty
        if (!empty($title))
        {

            # Variable is longer than 255 characters
            if(strlen($title) > 255)
            {
                $this->errorMsg[] = 10;
            }

        }
        # Error - empty variable
        else
        {
            $this->errorMsg[] = 11;
        }

    }

    private function validationContents($contents)
    {

        # Variable is empty
        if (empty($contents))
        {
            $this->errorMsg[] = 3;
        }

    }

    private function validationStatus($status)
    {

        # Variable has no good value
        if ($status != 0 and $status != 1)
        {
            $this->errorMsg[] = 12;
        }

    }

    /*
     *
     * Action functions
     *
     */

    # Function to add new post
    public function add ($title, $author, $status, $contents)
    {

        # Validation data
        $this->validationAuthor($author);
        $this->validationTitle($title);
        $this->validationStatus($status);
        $this->validationContents($contents);

        # No Errors
        if (count($this->errorMsg) == 0)
        {

            $this->title = $title;
            $this->author = $author;
            $this->status = $status;
            $this->contents = $contents;

            # Added new post
            if ($this->save()) return true;
            # Error
            else
            {
                $this->errorMsg[] = 13;
                return false;
            }

        }
        # Error
        else return false;

    }

    # Function to edit exist post
    public function edit ($title, $author, $status, $contents)
    {

        # Checking changes
        if ($this->status != $status or $this->title != $title or $this->author != $author or $this->contents != $contents)
        {

            # Validation data
            $this->validationAuthor($author);
            $this->validationTitle($title);
            $this->validationStatus($status);
            $this->validationContents($contents);

            # No Errors
            if (count($this->errorMsg) == 0)
            {

                $this->status = $status;
                $this->title = $title;
                $this->author = $author;
                $this->contents = $contents;

                # Edited post
                if ($this->save()) return true;
                # Error
                else
                {
                    $this->errorMsg[] = 16;
                    return false;
                }

            }
            # Error
            else return false;

        }
        # No changes
        else return true;

    }

    # Function to delete post
    public function delete ()
    {

        $this->dbHandle->bind("idPost", $this->id);
        $result = $this->dbHandle->query("DELETE FROM `post` WHERE `id` = :idPost;");

        # Deleted post
        if (count($result) == 1) return true;
        # Error
        else return false;

    }

    # Function to saving changes
    private function save ()
    {

        $result = null;

        # Save edit
        if ($this->id != null)
        {

            $this->dbHandle->bind("idPost",$this->id);
            $this->dbHandle->bind("title",$this->title);
            $this->dbHandle->bind("author",$this->author);
            $this->dbHandle->bind("contents",$this->contents);
            $this->dbHandle->bind("status",$this->status);

            $result = $this->dbHandle->query("UPDATE `post` SET `title` = :title, `author` = :author, `contents` = :contents, `status` = :status WHERE `post`.`id` = :idPost;");

        }
        # Save add new post
        else
        {

            $this->dbHandle->bind("title",$this->title);
            $this->dbHandle->bind("author",$this->author);
            $this->dbHandle->bind("contents",$this->contents);
            $this->dbHandle->bind("status",$this->status);

            $result = $this->dbHandle->query("INSERT INTO `post` (`id`, `title`, `author`, `contents`, `date`, `status`) VALUES (NULL, :title, :author, :contents, CURRENT_TIMESTAMP, :status);");

        }

        if ($result == 1) return true;
        else return false;

    }

}