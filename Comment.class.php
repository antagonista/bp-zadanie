<?php
/**
 * Author: Maksym Kawelski
 * Date: 15.09.18
 * Time: 18:31
 *
 * Comment class object
 *
 */

class Comment
{

    # Comment.class variables
    private $id = null;
    private $idPost = null;
    private $author = null;
    private $email = null;
    private $contents = null;
    private $date = null;

    # Helping variables
    private $gotComment = null; # true when comment by id exist; false when not
    private $errorMsg = Array(); # Errors array
    private $dbHandle = null; # DB handle


    public function __construct($idComment = null)
    {

        global $dbHandle;

        $this->dbHandle = $dbHandle;

        # Id comment sets
        if ($idComment != null)
        {

            # Check id comment
            $this->dbHandle->bind("idComment",$idComment);
            $comment = $this->dbHandle->query("SELECT * FROM `comment` WHERE `id` = :idComment");

            # Comment.class exist
            if (count($comment) == 1)
            {

                $this->id = $comment[0]['id'];
                $this->idPost = $comment[0]['id_post'];
                $this->author = $comment[0]['author'];
                $this->email = $comment[0]['e_mail'];
                $this->contents = $comment[0]['contents'];
                $this->date = $comment[0]['date'];

                $this->gotComment = true;

            }
            # Error - Comment.class don't exist
            else
            {
                $this->gotComment = false;
            }

        }

    }

    /*
     *
     * Get data functions
     *
     */

    public function getId ()
    {
        return $this->id;
    }

    public function getIdPost ()
    {
        return $this->idPost;
    }

    public function getAuthor ()
    {
        return $this->author;
    }

    public function getDate ()
    {
        return $this->date;
    }

    public function getEmail ()
    {
        return $this->email;
    }

    public function getContents ()
    {
        return $this->contents;
    }

    public function gotComment ()
    {
        return $this->gotComment;
    }

    public function getErrorMsg ()
    {
        return $this->errorMsg;
    }

    /*
     *
     * Validation data functions
     *
     */

    private function validationEmail ($email)
    {

        # Variable is not empty
        if (!empty($email))
        {

            # Check e-mail
            if(!preg_match('/^[a-zA-Z0-9\.\-_]+\@[a-zA-Z0-9\.\-_]+\.[a-z]{2,4}$/D', $email))
            {
                $this->errorMsg[] = 2;
            }

        }
        # Variable is empty
        else
        {
            $this->errorMsg[] = 1;
        }

    }

    private function validationAuthor ($author)
    {

        # Variable is not empty
        if (!empty($author))
        {

            # Variable is longer than 50 characters
            if(strlen($author) > 50)
            {
                $this->errorMsg[] = 4;
            }

        }
        # Variable is empty
        else
        {
            $this->errorMsg[] = 5;
        }

    }

    private function validationContent ($content)
    {

        # Variable is empty
        if (empty($content))
        {
            $this->errorMsg[] = 3;
        }

    }

    /*
     *
     * Action functions
     *
     */

    # Function to add new comment to post
    public function add ($idPost, $author, $email, $contents)
    {

        # Check PostId
        $this->dbHandle->bind("idPost",$idPost);
        $result = $this->dbHandle->query("SELECT * FROM `post` WHERE `id` = :idPost");

        # Post.class exist
        if (count($result) == 1)
        {

            # validation data
            $this->validationAuthor($author);
            $this->validationEmail($email);
            $this->validationContent($contents);

            # No errors - add new comments to db
            if (count($this->errorMsg) == 0)
            {

                $this->dbHandle->bind("idPost", $idPost);
                $this->dbHandle->bind("author", $author);
                $this->dbHandle->bind("email", $email);
                $this->dbHandle->bind("contents", $contents);

                $result = $this->dbHandle->query("INSERT INTO `comment` (`id`, `id_post`, `author`, `e_mail`, `date`, `contents`) VALUES (NULL, :idPost, :author, :email, CURRENT_TIMESTAMP, :contents)");

                # Added to db
                if ($result == 1)
                {
                    return true;
                }
                # Error adding to db
                else
                {
                    $this->errorMsg = 6;
                    return false;
                }

            }
            # Errors exist
            else
            {
                return false;
            }

        }
        # Post.class don't exist
        {
            return false;
        }


    }


}