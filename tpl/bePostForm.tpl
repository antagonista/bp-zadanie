<h1>{var:actionName} postu</h1>

    <div id="error">{var:errorMsg}</div>

    <form method="get">

        <input type="hidden" name="dir" value="backend">
        <input type="hidden" name="action" value="{var:action}">
        <input type="hidden" name="idPost" value="{var:idPost}">

        <table>

            <tr>
                <td>
                    Tytuł:
                </td>
                <td>
                    <input type="text" name="title" value="{var:title}">
                </td>
            </tr>

            <tr>
                <td>
                    Autor:
                </td>
                <td>
                    <input type="text" name="author" value="{var:author}">
                </td>
            </tr>

            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <select name="status">
                        <option value="1" {var:pub}>Opublikowany</option>
                        <option value="0" {var:notpub}>Nieopublikowany</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    Treść:
                </td>
                <td>
                    <textarea name="contents">{var:contents}</textarea>
                </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <input type="submit" value="Wyślij">
                </td>
            </tr>

        </table>

    </form>