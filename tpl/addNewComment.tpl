<h1>Dodawanie nowego komentarza</h1>


<div id="error">
    {var:errorMsg}
</div>

<form method="get">

    <input type="hidden" name="post" value="{var:idPost}">
    <input type="hidden" name="action" value="addComment">

    <table>

        <tr>
            <td>
                Autor:
            </td>
            <td>
                <input type="text" name="author" value="{var:author}">
            </td>
        </tr>

        <tr>
            <td>
                Adres e-mail:
            </td>
            <td>
                <input type="email" name="email" value="{var:email}">
            </td>
        </tr>

        <tr>
            <td>
                Treść:
            </td>
            <td>
                <textarea name="contents">{var:contents}</textarea>
            </td>
        </tr>

        <tr>
            <td></td>
            <td>
                <input type="submit" value="Dodaj">
            </td>
        </tr>


    </table>

</form>