<!DOCTYPE html>
<html lang="pl">
<head>
    <title>BP</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" href="/{var:tplDir}styles.css">
</head>
<body>

    <nav>
        <a href="/">FrontEnd</a>
        <a href="/?dir=backend">BackEnd</a>
    </nav>

    {var:contentSite}

</body>
</html>