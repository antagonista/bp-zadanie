<h1>Zarządzanie postami</h1>

<div id="msgBox">{var:msg}</div>

<a href="?dir=backend&action=addNewPost">Dodaj nowy post</a>

<script>

    function deletePost (idPost, titlePost)
    {
        if (window.confirm("Czy na pewno usunąć post o tytule '" + titlePost + "' ?"))
            {
                window.location.replace("?dir=backend&action=delPost&idPost=" + idPost);
            }
    }


</script>


<table id="listPost">

    <tr>
        <td>Tytuł</td>
        <td>Data dodania</td>
        <td>Status</td>
        <td></td>
        <td></td>
    </tr>

    {var:listPost}

</table>