<?php
/**
 * Author: Maksym Kawelski
 * Date: 15.09.18
 * Time: 14:39
 *
 * Main site controller
 *
 */

header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 'On');

# zalaczanie bibliotek
require_once "TPL.class.php";
require_once "Db.class.php";
require_once "Post.class.php";
require_once "Comment.class.php";

# Config
$tplDir = "tpl/";

$dbInfo = Array();
$dbInfo["host"] = "localhost";
$dbInfo["user"] = "root";
$dbInfo["pass"] = "K0rek1";
$dbInfo["nameDB"] = "rekrutacja_blog";

# Set DB connection
$dbHandle = new \PDOWrapper\DB($dbInfo["host"],$dbInfo["nameDB"],$dbInfo["user"],$dbInfo["pass"]);
$dbHandle->query("SET CHARSET utf8");

# Set main TPL.class
$mainTPL = new TPL($tplDir."main.tpl");
$mainTPL->setVariable("tplDir",$tplDir);

# Set dictionary tpl
$tplMsg = Array();
$tplMsg[1] = "Pole adresu e-mail nie moze byc puste<br>";
$tplMsg[2] = "Nie podales prawidlowego adresu e-mail<br>";
$tplMsg[3] = "Pole treści nie może być puste<br>";
$tplMsg[4] = "Pole autor nie moze zawierac wiecej niz 50 znakow<br>";
$tplMsg[5] = "Pole autor nie moze byc puste<br>";
$tplMsg[6] = "Błąd dodawania komentarza do bazy danych<br>";
$tplMsg[7] = "Opublikowany";
$tplMsg[8] = "Nieopublikowany";
$tplMsg[9] = "Dodawanie nowego";
$tplMsg[10] = "Pole tytułu nie moze zawierac wiecej niz 255 znakow<br>";
$tplMsg[11] = "Pole tytułu nie moze byc puste<br>";
$tplMsg[12] = "Status postu może być co jedynie opublikowany lub nieopublikowany<br>";
$tplMsg[13] = "Błąd dodania nowego postu do bazy danych<br>";
$tplMsg[14] = "Edycja";
$tplMsg[15] = "Nie udało się usunąć postu<br>";
$tplMsg[16] = "Błąd edycji postu w bazie danych<br>";
$tplMsg[17] = "Pomyślnie usunięto post<br>";
$tplMsg[18] = "Pomyślnie edytowano post<br>";
$tplMsg[19] = "Pomyślnie dodano nowy post<br>";
$tplMsg[20] = "Błąd próby edycji postu<br>";

/*
 * Helping functions
 */

# Redirect to indicated address on site
function redirectTo ($dir)
{

    if ($dir == "main") $dir = "../";

    header("Location: ".$dir);
    die();

}

/*
 * GET_ variables support
 */

$dir = (isset($_GET['dir'])) ? $_GET['dir'] : null;
$action = (isset($_GET['action'])) ? $_GET['action'] : null;
$post = (isset($_GET['post'])) ? $_GET['post'] : null;
$idPost = (isset($_GET['idPost'])) ? $_GET['idPost'] : null;
$title = (isset($_GET['title'])) ? $_GET['title'] : null;
$email = (isset($_GET['email'])) ? $_GET['email'] : null;
$author = (isset($_GET['author'])) ? $_GET['author'] : null;
$status = (isset($_GET['status'])) ? $_GET['status'] : null;
$contents = (isset($_GET['contents'])) ? $_GET['contents'] : null;

/*
 *
 * Controllers
 *
 */


if ($dir!=null and $dir=="backend")
    # BackEnd
{

    # Add new post
    if ($action == "addNewPost")
    {

        # Sent form
        if ( ($title != null) or ($author != null) or ($status != null) or ($contents != null) )
        {

            # Set Post.class Object
            $post= new Post();

            # Added new post to db
            if ($post->add($title,$author,$status,$contents))
            {

                # Redirect to BackEnd panel with message
                redirectTo("?dir=backend&action=addedNewPost");

            }
            # Error - show form with user data and error msg
            else
            {

                # Set TPL.class
                $formTPL = new TPL($tplDir."bePostForm.tpl");

                # Set TPL.class variables
                $formTPL->setVariable("actionName",$tplMsg[9]);
                $formTPL->setVariable("action","addNewPost");
                $formTPL->setVariable("idPost",null);

                $errorsMsg = null;

                # If are errors
                if (count($post->getErrorMsg()) != 0)
                {

                    # Listing errors
                    foreach ($post->getErrorMsg() as $error)
                    {
                        $errorsMsg .= $tplMsg[$error];
                    }

                }

                # Set TPL.class variables
                $formTPL->setVariable("errorMsg",$errorsMsg);
                $formTPL->setVariable("title",$title);
                $formTPL->setVariable("author",$author);
                $formTPL->setVariable("contents",$contents);

                if ($status == 1)
                {
                    $formTPL->setVariable("pub","selected");
                }
                else
                {
                    $formTPL->setVariable("notpub","selected");
                }

                # Set main TPL.class
                $mainTPL->setVariable("contentSite", $formTPL->renderView());

            }

        }
        # Empty form
        else
        {

            # Set TPL.class
            $formTPL = new TPL($tplDir."bePostForm.tpl");

            # Set TPL.class variables
            $formTPL->setVariable("actionName",$tplMsg[9]);
            $formTPL->setVariable("action","addNewPost");
            $formTPL->setVariable("idPost",null);

            # Set main TPL.class
            $mainTPL->setVariable("contentSite", $formTPL->renderView());

        }

    }
    # Edit post
    elseif ( $action == "editPost" and $idPost != null )
    {

        # Sent form
        if ($title != null or $author != null or $status != null or $contents != null)
        {

            # Set Post.class Object
            $post = new Post($idPost);

            # Post.class exist
            if ($post->gotPost())
            {

                # Edited post
                if ($post->edit($title,$author,$status,$contents))
                {

                    # Redirect to BackEnd panel with message
                    redirectTo("?dir=backend&action=editedPost");

                }
                # Error - show form with user data and errors msg
                else
                {

                    # Set TPL.class
                    $formTPL = new TPL($tplDir."bePostForm.tpl");

                    $errorsMsg = null;

                    # If are errors
                    if (count($post->getErrorMsg()) != 0)
                    {
                        foreach ($post->getErrorMsg() as $error)
                        {
                            $errorsMsg .= $tplMsg[$error];
                        }

                    }

                    # Set TPL.class variables
                    $formTPL->setVariable("errorMsg",$errorsMsg);
                    $formTPL->setVariable("actionName", $tplMsg[14]);
                    $formTPL->setVariable("action", "editPost");
                    $formTPL->setVariable("idPost", $post->getId());
                    $formTPL->setVariable("title", $title);
                    $formTPL->setVariable("author", $author);
                    $formTPL->setVariable("contents", $contents);

                    if ($status == 1)
                    {
                        $formTPL->setVariable("pub","selected");
                    }
                    else
                    {
                        $formTPL->setVariable("notpub","selected");
                    }

                    # Set main TPL.class
                    $mainTPL->setVariable("contentSite", $formTPL->renderView());

                }

            }
            # Error
            else
            {

                # Redirect to BackEnd panel with message
                redirectTo("?dir=backend&action=errorEditPost");

            }

        }
        # Empty form
        else
        {

            # Set Post.class object
            $post = new Post($idPost);

            # Post.class exist
            if ($post->gotPost())
            {

                # Set TPL.class
                $formTPL = new TPL($tplDir."bePostForm.tpl");

                # Set TPL.class variables
                $formTPL->setVariable("actionName", $tplMsg[14]);
                $formTPL->setVariable("action", "editPost");
                $formTPL->setVariable("idPost", $post->getId());
                $formTPL->setVariable("title", $post->getTitle());
                $formTPL->setVariable("author", $post->getAuthor());
                $formTPL->setVariable("contents", $post->getContents());

                if ($post->getStatus() == 1)
                {
                    $formTPL->setVariable("pub","selected");
                }
                else
                {
                    $formTPL->setVariable("notpub","selected");
                }

                # Set main TPL.class
                $mainTPL->setVariable("contentSite", $formTPL->renderView());

            }
            # Error
            else
            {

                # Redirect to BackEnd panel with message
                redirectTo("?dir=backend&action=errorEditPost");

            }


        }

    }
    # Delete post
    elseif ($action == "delPost" and $idPost != null)
    {

        # Set Post.class object
        $post = new Post($idPost);

        # Post.class exist
        if ($post->gotPost())
        {

            # Post.class deleted
            if ($post->delete())
            {

                # Redirect to BackEnd panel with message
                redirectTo("?dir=backend&action=deletedPost");

            }
            # Error
            else
            {

                # Redirect to BackEnd panel with message
                redirectTo("?dir=backend&action=errorDeletedPost");

            }

        }
        # Post.class don't exist
        else
        {

            # Redirect to BackEnd panel with message
            redirectTo("?dir=backend&action=errorDeletedPost");

        }

    }
    # BackEnd panel
    else
    {

        # Set TPL.class
        $BEPanelTPL = new TPL($tplDir."beListPost.tpl");

        # Action messages
        if ($action == "addedNewPost" or $action == "editedPost" or $action == "delPost")
        {

            if ($action == "addedNewPost") $BEPanelTPL->setVariable("msg",$tplMsg[19]);
            if ($action == "editedPost") $BEPanelTPL->setVariable("msg",$tplMsg[18]);
            if ($action == "deletedPost") $BEPanelTPL->setVariable("msg",$tplMsg[17]);
            if ($action == "errorDeletedPost") $BEPanelTPL->setVariable("msg",$tplMsg[15]);
            if ($action == "errorEditPost") $BEPanelTPL->setVariable("msg",$tplMsg[20]);

        }

        # Get all posts from db
        $postListId = $dbHandle->query("SELECT `id` FROM `post` ORDER BY `date` DESC");

        # Posts exist
        if (count($postListId) != 0)
        {

            $tempPostTPL = null;

            # Listing posts id
            foreach ($postListId as $element)
            {

                # Set post Object
                $postObj = new Post($element['id']);

                # Post.class exist
                if ($postObj->gotPost())
                {

                    # Set single post TPL.class
                    $singlePost = new TPL($tplDir . "beSinglePost.tpl");

                    $singlePost->setVariable("title", $postObj->getTitle());
                    $singlePost->setVariable("date", $postObj->getDate());
                    $singlePost->setVariable("idPost", $postObj->getId());

                    if ($postObj->getStatus() == 1) $singlePost->setVariable("status", $tplMsg[7]);
                    else $singlePost->setVariable("status", $tplMsg[8]);

                    # Add single post tpl to temp tpl
                    $tempPostTPL .= $singlePost->renderView();

                }

            }

            # Set single posts tpls to sub main tpl
            $BEPanelTPL->setVariable("listPost",$tempPostTPL);

        }

        # Set main TPL.class
        $mainTPL->setVariable("contentSite", $BEPanelTPL->renderView());

    }

}
else
    # FrontEnd
{

    # Show post with comments or add new comment
    if ($post != null)
    {

        # Add new comment
        if ($action != null and $action == "addComment")
        {

            # Sent form
            if ($author != null or $email != null or $contents != null) # Data processing from the form
            {

                # Set comment Obj
                $comment = new Comment();

                # Added to db
                if ($comment->add($post,$author,$email,$contents))
                {

                    # Redirect to show post with comments
                    redirectTo("?post=".$post);

                }
                # Error - show form with error msg
                else
                {

                    # Set TPL.class form add new comment with data user and error msg
                    $newCommentTPL = new TPL($tplDir."addNewComment.tpl");
                    $newCommentTPL->setVariable("idPost",$post);

                    $msg = null;

                    # If are errors
                    if (count($comment->getErrorMsg()) != 0)
                    {

                        # Listing errors
                        foreach ($comment->getErrorMsg() as $error)
                        {
                            $msg .= $tplMsg[$error];
                        }

                    }

                    # Set variables to tpl
                    $newCommentTPL->setVariable("errorMsg",$msg);
                    $newCommentTPL->setVariable("author",$author);
                    $newCommentTPL->setVariable("email",$email);
                    $newCommentTPL->setVariable("contents",$contents);

                    # Set to the main template
                    $mainTPL->setVariable("contentSite", $newCommentTPL->renderView());

                }

            }
            # Show empty form
            else
            {

                # Set TPL.class form add new comment
                $newCommentTPL = new TPL($tplDir."addNewComment.tpl");

                # Set variable to tpl
                $newCommentTPL->setVariable("idPost",$post);

                # Set to the main template
                $mainTPL->setVariable("contentSite", $newCommentTPL->renderView());

            }

        }
        # Show post with comments
        else
        {

            # Set TPL.class
            $postWithCommentsTPL = new TPL($tplDir . "postWithComments.tpl");

            # Get post Obj
            $postObj = new Post($post);

            # Post.class exist
            if ($postObj->gotPost())
            {

                # Render post contents
                $postTPL = new TPL($tplDir . "singlePost.tpl");

                # Set variables to tpl
                $postTPL->setVariable("id", $postObj->getId());
                $postTPL->setVariable("title", $postObj->getTitle());
                $postTPL->setVariable("author", $postObj->getAuthor());
                $postTPL->setVariable("date", $postObj->getDate());
                $postTPL->setVariable("contents", $postObj->getContents());

                # Add post data to sub-main TPL.class
                $postWithCommentsTPL->setVariable("postContents", $postTPL->renderView());
                $postWithCommentsTPL->setVariable("idPost", $postObj->getId());

                # Render comments contents
                $commentsTPL = null;

                # Get data from DB
                $dbHandle->bind("idPost", $postObj->getId());
                $commentsIds = $dbHandle->query("SELECT `id` FROM `comment` WHERE `id_post` = :idPost");

                # Comments exist for post
                if (count($commentsIds) >= 1)
                {

                    $commentsTempTPL = null;

                    # Listing comments
                    foreach ($commentsIds as $comment) {

                        # Get comment contents
                        $commentObj = new Comment($comment['id']);

                        # Comment.class exist
                        if ($commentObj->gotComment())
                        {

                            # Set comment TPL.class
                            $commentTPL = new TPL($tplDir . "singleComment.tpl");

                            $commentTPL->setVariable("author", $commentObj->getAuthor());
                            $commentTPL->setVariable("email", $commentObj->getAuthor());
                            $commentTPL->setVariable("date", $commentObj->getAuthor());
                            $commentTPL->setVariable("contents", $commentObj->getAuthor());

                            # Set single comments tpl to temp tpl
                            $commentsTempTPL .= $commentTPL->renderView();

                        }

                    }

                    # Add renders comments to sub-main TPL.class
                    $postWithCommentsTPL->setVariable("comments", $commentsTempTPL);

                }

            }
            # Post class don't exist
            else
            {

                # Error - redirect to main site
                redirectTo("main");

            }

            # Set to the main template
            $mainTPL->setVariable("contentSite", $postWithCommentsTPL->renderView());

        }

    }
    # Show all posts
    else
    {

        $postsView = null;

        # Get all posts from DB
        $postsListId = $dbHandle->query("SELECT `id` FROM `post` WHERE `status` = 1 ORDER BY `date` DESC");

        # Listing posts
        foreach ($postsListId as $element) {

            # Set post Object
            $postObj = new Post($element['id']);

            # Post.class exist
            if ($postObj->gotPost())
            {

                # Set post TPL.class
                $postTPL = new TPL($tplDir . "singlePost.tpl");

                # Set TPL.class variables
                $postTPL->setVariable("id", $postObj->getId());
                $postTPL->setVariable("title", $postObj->getTitle());
                $postTPL->setVariable("author", $postObj->getAuthor());
                $postTPL->setVariable("date", $postObj->getDate());
                $postTPL->setVariable("contents", $postObj->getContents());

                # Set single post tpl to temp tpl
                $postsView .= $postTPL->renderView();

            }

        }

        # Set to the main template
        $mainTPL->setVariable("contentSite", $postsView);

    }

}

# Render site
echo $mainTPL->renderView();

