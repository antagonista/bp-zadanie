-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 17 Wrz 2018, 22:51
-- Wersja serwera: 10.1.32-MariaDB
-- Wersja PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `rekrutacja_blog`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contents` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `comment`
--

INSERT INTO `comment` (`id`, `id_post`, `author`, `e_mail`, `date`, `contents`) VALUES
(1, 3, 'autor kom 1', 'mail@mail.com', '2018-09-15 15:40:02', 'text kom 1'),
(2, 3, 'autor', 'email', '2018-09-15 20:48:22', 'content'),
(3, 3, 'aut', 'mail@mail.com', '2018-09-15 20:54:52', 'aaaaa'),
(4, 3, 'aaasd', 'mail@mail.com', '2018-09-15 20:56:12', 'asdas'),
(5, 3, 'das', 'mail@mail.com', '2018-09-15 20:57:03', 'asdasd'),
(6, 5, 'nowy', 'mail@mail.com', '2018-09-15 20:57:34', 'nowy kom'),
(7, 5, 'nowy 2', 'mail@mail.com', '2018-09-15 21:14:18', 'nowy 2'),
(8, 5, 'f', 'f@g.com', '2018-09-16 11:57:01', 'f'),
(9, 5, 'aa', 'mail@mail.com', '2018-09-16 11:57:27', 'ma'),
(19, 17, 'a', 'mail@mail.com', '2018-09-17 14:27:30', 'a');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contents` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL COMMENT '0 - not published; 1 - published'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `post`
--

INSERT INTO `post` (`id`, `title`, `author`, `contents`, `date`, `status`) VALUES
(3, 'taki tam post', 'autor 1', 'text 1', '2018-09-15 15:27:44', 1),
(5, 'post 3', 'autor 3', 'text 3', '2018-09-15 16:29:50', 1),
(6, 'title', 'author', 'contents', '2018-09-16 15:56:47', 1),
(8, 'tego edytowalem drugi raz', 'autor 2', 'tresc 2', '2018-09-16 16:06:49', 0),
(9, 'Nowy aćę', 'Autor', 'ąćę', '2018-09-16 16:26:59', 0),
(10, 'ąćę', 'ąćę', 'ąćę', '2018-09-16 17:00:28', 1),
(12, 'ąćę', 'ąćę', 'sa', '2018-09-16 17:05:30', 1),
(14, 'te', 'te', 'ss', '2018-09-17 13:07:53', 1),
(15, 'nowy', 'taki', 'treść', '2018-09-17 13:08:05', 1),
(16, 'ste', 'tes', 'tes', '2018-09-17 13:55:15', 0),
(17, 'nowy', 'nowy', 'nowy', '2018-09-17 13:55:44', 1),
(19, 'te', 'tesss', 'te', '2018-09-17 14:27:05', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_post` (`id_post`);

--
-- Indeksy dla tabeli `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT dla tabeli `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `id_post` FOREIGN KEY (`id_post`) REFERENCES `post` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
