<?php
/**
 * Author: Maksym Kawelski
 * Date: 15.09.18
 * Time: 14:57
 *
 * Simple Template Engine
 *
 */

class TPL
{

    # Object variables
    private $tplFile = null;
    private $tplContent = null;
    private $variablesName = Array();
    private $variablesValue = Array();
    private $substitutionUndefinedVar = true; #default

    public function __construct($tplFileName, $substitutionUndefinedVar = null)
    {

        # TPL.class filename can not be empty
        if (!empty($tplFileName))
        {

            $this->tplFile = $tplFileName;

            # Check file - exist
            if (file_exists($this->tplFile))
            {

                # Get file content
                $this->tplContent = file_get_contents($this->tplFile);

            }
            # TPL.class file don't exist
            else
            {
                die("ERROR: TPL.class File is not exists.");
            }

        }
        # Error - TPL.class filename is empty
        else
        {
            die("ERROR: TPL.class File Name is not defined");
        }

        # Check change unset values
        if ($substitutionUndefinedVar != null)
        {
            if (is_bool($substitutionUndefinedVar))
            {

                $this->substitutionUndefinedVar = $substitutionUndefinedVar;

            }
        }

    }

    /*
     *
     * Action function
     *
     */

    # Set variable in TPL.class
    public function setVariable($name, $value)
    {

        $this->variablesName[] = "{var:".$name."}";
        $this->variablesValue[] = $value;

    }

    # Replace variables
    private function replaceVariables($arrayName, $arrayValue)
    {

        $this->tplContent = str_replace($arrayName,$arrayValue,$this->tplContent);

    }

    # Render TPL.class View
    public function renderView()
    {

        # Replace defined variables
        $this->replaceVariables($this->variablesName,$this->variablesValue);

        # Replace undefined variables
        if ($this->substitutionUndefinedVar == true)
        {

            $result = null;

            # Search not replaced variables
            preg_match_all("/{var:(.*)}/",$this->tplContent,$result);

            # Not replaced variables exist
            if (!empty($result[0]))
            {

                # Replace on empty value
                $this->replaceVariables($result[0],"");

            }

        }

        # Return ready TPL.class
        return $this->tplContent;

    }


}


